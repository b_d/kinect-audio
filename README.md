# Tests Kinect capabilities and lets you play some air drums!


Project „Kinect Audio“ is designed to test the possibilities of the Kinect sensor and thereby make an air drum app. It is made in Visual Studio 2013 community edition, using C# for code behind and Windows Presentation Foundation for the UI.

The application is based on the use of the skeleton stream of Kinect for detecting the user’s skeleton. Each second comes with 30 new frames and for each frame there is detection of the right and the left hand of the user. Furthermore, for each frame lines are drawn, representing the user’s skeleton.

X and Y coordinates of each hand are used to set attributes of an object that is instance of AudioRectangle class. This is the main class where all the logic of the app is and besides hands variables, it has a list of squares that represent the area in which user needs to put his hand to start the audio playback. Each rectangle is connected to its own audio file that represents different drum sound.

When the user presses the space key he enters the mode for changing the position of the rectangles. Then the first two rectangles, in which the user enters, become sticky rectangles and they stick to the position of the user’s hands, till the space key is pressed again. This enables user to arrange rectangles how he likes best. Also, when the user press the J, K or L key, a different song starts to play, so the user can drum along it. 

# Additional info
[APASLab](http://apaslab.riteh.hr/project-summary-kinect-audio/)

# Thanks
Thanks for [MusicRadar](http://www.musicradar.com/) for droum loops, [MinusDrums](http://minusdrums.com/joomla/index.php) for backround music and Microsoft for Visual Studio, Kinect SDK and samples.